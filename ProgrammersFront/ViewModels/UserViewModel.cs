﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProgrammersFront.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public int UserId { get; set; }
        [Required(ErrorMessage = "Preencha o campo nome")]
        [MaxLength(50, ErrorMessage = "Valor maximo de 50 caracteres")]
        [MinLength(2, ErrorMessage = "Minímo de 2 caracteres")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Preencha o campo email")]
        [MaxLength(50, ErrorMessage = "Valor maximo de 100 caracteres")]
        [EmailAddress(ErrorMessage = "Preencha um e-mail valido")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Preencha o campo telefone")]
        [MaxLength(50, ErrorMessage = "Valor maximo de 20 caracteres")]
        public string Phone { get; set; }
    }
}
