﻿using Microsoft.AspNetCore.Mvc;
using ProgrammersFront.ViewModels;
using System.Collections.Generic;
using AutoMapper;
using Core.Domain.Entities;
using Core.Application.Interfaces;

namespace ProgrammersFront.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserAppService _userApp;
        private readonly IMapper _mapper;

        public UserController(IUserAppService userApp, IMapper mapper)
        {
            _userApp = userApp;
            _mapper = mapper;
        }

        // GET: api/Users
        [HttpGet]
        [Route("api/Users")]
        public IEnumerable<UserViewModel> GetUsers()
        {
            var listUser = _mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>( _userApp.GetAll());

            return listUser;
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        [Route("api/Users/{id}")]
        public IActionResult GetUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _mapper.Map<User, UserViewModel>( _userApp.GetById(id));
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        [Route("api/Users/{id}")]
        public IActionResult PutUser([FromRoute] int id, [FromBody] UserViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != userViewModel.UserId)
            {
                return BadRequest();
            }
            else
            {
                 _userApp.Update(_mapper.Map<UserViewModel, User>(userViewModel));
            }
        
            return NoContent();
        }

        // POST: api/User
        [HttpPost]
        [Route("api/Users/")]
        public IActionResult PostUser([FromBody] UserViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

             _userApp.Add(_mapper.Map<UserViewModel, User>(userViewModel));
           
            return CreatedAtAction("GetContato", new { id = userViewModel.UserId }, userViewModel);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        [Route("api/Users/{id}")]
        public IActionResult DeleteUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _mapper.Map<User, UserViewModel>( _userApp.GetById(id));
            var removeUser = _mapper.Map <UserViewModel, User>(user);

            if (removeUser == null)
            {
                return NotFound();
            }    
            return Ok(removeUser);
        }

    }
}