﻿using AutoMapper;
using Core.Domain.Entities;
using ProgrammersFront.ViewModels;

namespace ProgrammersFront.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<UserViewModel, User>();                     
        }
    }
}
