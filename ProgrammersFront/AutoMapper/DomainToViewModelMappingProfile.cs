﻿using AutoMapper;
using Core.Domain.Entities;
using ProgrammersFront.ViewModels;

namespace ProgrammersFront.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
           CreateMap<User,UserViewModel>();
        }
    }
}