﻿import React, { Component } from 'react';
import axios from 'axios';

export class User extends Component {
  
    constructor(props) {
        super(props);
        this.state = { listUser: [] }
        this.LisUsers()
    }

      LisUsers(){      
          axios.get(`api/Users`)
            .then(response => { this.setState({ listUser: response.data }); })
            .catch(() => { console.log('Erro ao recuperar os dados'); });
    }

    render() {
        return (
            <div>
                {this.state.listUser.map(function (item) { console.log(item) })}
            </div>
        );
    }
}
