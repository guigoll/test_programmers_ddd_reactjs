﻿using Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application.Interfaces
{
    public interface IUserAppService : IAppServiceBase<User>
    {
        IEnumerable<User> SearchUser(string email);
    }
}
