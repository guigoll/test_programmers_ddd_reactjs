﻿using Core.Application.Interfaces;
using Core.Domain.Entities;
using Core.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application
{
   public  class UserAppService : AppServiceBase<User>, IUserAppService
    {
        private readonly IUserService _userService;

        public UserAppService(IUserService userService)
            :base(userService)
        {
            _userService = userService;
        }

        public IEnumerable<User> SearchUser(string email)
        {
            return _userService.SearchUser(email);
        }
    }
}
