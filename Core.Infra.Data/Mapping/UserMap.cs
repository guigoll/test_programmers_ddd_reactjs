﻿using Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Infra.Data.Mapping
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //Table
            builder.ToTable("User");

            // Chave PK
            builder.HasKey("UserId");

            // Atributos
            builder.Property(u => u.Name)
                .HasColumnName("Name")
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(u => u.Email)
                .HasColumnName("Email")
                .HasColumnType("varchar(100)")
                .IsRequired();

            builder.Property(u => u.Phone)
                .HasColumnName("Phone")
                .HasColumnType("varchar(20)")
                .IsRequired();
        }
    }
}
