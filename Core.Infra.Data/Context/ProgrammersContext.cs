﻿using Core.Domain.Entities;
using Core.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace Core.Infra.Data.Context
{
    public class ProgrammersContext : DbContext
    {
        public ProgrammersContext(DbContextOptions<ProgrammersContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                 .Build();
                optionsBuilder.UseNpgsql(configuration.GetConnectionString("Programmer"));
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region U
            modelBuilder.Entity<User>(new UserMap().Configure);
            #endregion

        }

        #region U
        public DbSet<User> User { get; set; }
        #endregion


    }
}
