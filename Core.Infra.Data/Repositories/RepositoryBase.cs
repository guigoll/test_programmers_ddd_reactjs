﻿using Core.Domain.Interfaces;
using Core.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Core.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly ProgrammersContext _Db;
        protected readonly DbSet<TEntity> DbSet;

        public RepositoryBase(ProgrammersContext Db)
        {
            _Db = Db;
            DbSet = Db.Set<TEntity>();
        }

        public void Add(TEntity obj)
        {
            _Db.Set<TEntity>().Add(obj);
            _Db.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _Db.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return _Db.Set<TEntity>().Find(id);
        }

        public void Remove(TEntity obj)
        {
            _Db.Set<TEntity>().Remove(obj);
        }

        public void Update(TEntity obj)
        {
            _Db.Entry(obj).State = EntityState.Modified;
            _Db.SaveChanges();
        }
        public void Dispose()
        {
            _Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
