﻿using Core.Domain.Entities;
using Core.Domain.Interfaces;
using Core.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace Core.Infra.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(ProgrammersContext Db)
            :base(Db)
        {
          
        }
        //Repositorio especifico
        public IEnumerable<User> SearchUser(string email)
        {
            return _Db.User.Where(x => x.Email == email);
        }
    }
}
