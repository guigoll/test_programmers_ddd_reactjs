﻿using Core.Domain.Entities;
using System.Collections.Generic;

namespace Core.Domain.Interfaces.Services
{
   public interface IUserService : IServiceBase<User>
    {
        IEnumerable<User> SearchUser(string email);
    }
}
