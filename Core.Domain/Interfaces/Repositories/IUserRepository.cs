﻿using Core.Domain.Entities;
using System.Collections.Generic;

namespace Core.Domain.Interfaces
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        IEnumerable<User> SearchUser(string email);
    }
}
