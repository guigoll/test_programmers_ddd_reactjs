﻿using Core.Domain.Entities;
using Core.Domain.Interfaces;
using Core.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Services
{
    public class UserService : ServiceBase<User>, IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
            :base(userRepository)
        {
            _userRepository = userRepository;
        }  

        public IEnumerable<User> SearchUser(string email)
        {
            return _userRepository.SearchUser(email);
        }
    }
}
